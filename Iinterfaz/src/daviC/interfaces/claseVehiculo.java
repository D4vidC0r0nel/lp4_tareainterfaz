/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daviC.interfaces;

/**
 *
 * @author David Coroneel
 */
public class claseVehiculo implements interfacerueda {
    private String marca;

    public claseVehiculo(String marca) {
        this.marca = marca;
    }

    public claseVehiculo() {
    }

    public void avanzar() {
        System.out.println("El vehiculo avanza ");
    }

    public void detenerse() {
        System.out.println("El vehiculo se detiene");
    }
    
}
